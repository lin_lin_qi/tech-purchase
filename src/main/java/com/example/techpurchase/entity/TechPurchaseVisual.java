package com.example.techpurchase.entity;

import lombok.Data;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/22
 */
@Data
public class TechPurchaseVisual {
    private String goodCategory;
    private Long goodCategoryNum;
}

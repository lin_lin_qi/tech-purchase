package com.example.techpurchase.entity;

import lombok.Data;

/**
 * @author 李乐雅
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/10 22:40
 */
@Data
public class MallUser {

    private Long mallUserId;

    private String mallUserName;

    private String mallUserPwd;

    public MallUser(String mallUserName, String mallUserPwd) {
        this.mallUserName = mallUserName;
        this.mallUserPwd = mallUserPwd;
    }

}


package com.example.techpurchase.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class AdminUser {
    private Long adminUserId;

    private String adminUserName;

    private String adminUserPwd;

    public AdminUser(String adminUserName, String adminUserPwd) {
        this.adminUserName = adminUserName;
        this.adminUserPwd = adminUserPwd;
    }
}
package com.example.techpurchase.entity;

import lombok.Data;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/25
 */

@Data
public class TechPurchaseCount {
    private Long goodsCount;
}

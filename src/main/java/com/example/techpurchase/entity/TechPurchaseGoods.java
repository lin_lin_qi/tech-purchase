package com.example.techpurchase.entity;

import lombok.Data;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/12
 */

@Data
public class TechPurchaseGoods {
    private Long goodId;
    private String goodCategory;
    private String goodName;
    private Long purchaseUserId;
    private Long purchaseNum;
    private String purchaseState;


}


package com.example.techpurchase.exception;

public class TechPurchaseException extends RuntimeException {

    public TechPurchaseException() {
    }

    public TechPurchaseException(String message) {
        super(message);
    }

    /**
     * 丢出一个异常
     *
     * @param message
     */
    public static void fail(String message) {
        throw new TechPurchaseException(message);
    }

}

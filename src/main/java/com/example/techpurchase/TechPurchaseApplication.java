package com.example.techpurchase;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.techpurchase.dao")
public class TechPurchaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechPurchaseApplication.class, args);
    }

}

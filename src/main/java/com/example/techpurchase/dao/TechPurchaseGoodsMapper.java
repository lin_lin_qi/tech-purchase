package com.example.techpurchase.dao;

import com.example.techpurchase.entity.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/12
 */
public interface TechPurchaseGoodsMapper {

    /**
     * 添加商品
     * @param goods
     * @return
     */
    Long insert(TechPurchaseGoods goods);


    /**
     * 修改商品信息
     * @param goods
     * @return
     */
    Long updateById(TechPurchaseGoods goods);


    /**
     * 根据id删除商品
     * @param goodId
     * @return
     */
    Long deleteById(Long goodId);

    /**
     * 批量删除商品
     * @param goodIds
     * @return
     */
    Long deleteByIds(Long[] goodIds);

    /**
     * 商品类别和数量可视化
     * @return
     */
    List<TechPurchaseVisual> visual();

    /**
     * 全部商品的数量
     * @return
     */
    List<TechPurchaseCount> goodsCount();

    /**
     * 查询到的商品数量
     * @param techPurchaseQuery
     * @return
     */
    List<TechPurchaseCount> goodsCountByQuery(TechPurchaseQuery techPurchaseQuery);

    /**
     * 分页列出全部商品
     * @param techPurchasePage
     * @return
     */
    List<TechPurchaseGoods> goodsList(TechPurchasePage techPurchasePage);

    /**
     * 分页查询全部商品
     * @param techPurchasePageByQuery
     * @return
     */
    List<TechPurchaseGoods> goodsListByQuery(TechPurchasePageByQuery techPurchasePageByQuery);

    Long mallDeleteById(Long goodId, Long userId);

    Long mallDeleteByIds(Long[] goodIds, Long userId);
}

package com.example.techpurchase.dao;

import com.example.techpurchase.entity.AdminUser;
import com.example.techpurchase.entity.MallUser;

/**
 * @author 李乐雅
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/10 22:37
 */
public interface MallUserMapper {

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    long insert(MallUser user);

    /**
     * 根据用户名和密码查找用户
     *
     * @param name
     * @param pwd
     * @return
     */
    MallUser find(String name,String pwd);


    /**
     * 根据用户名查找是否存在同名用户
     *
     * @param name
     * @return
     */
    MallUser findByName(String name);

    MallUser selectByPrimaryKey(Long userId);
}

package com.example.techpurchase.dao;

import com.example.techpurchase.entity.MallUserToken;

/**
 * @author 李乐雅
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/10 22:55
 */
public interface MallUserTokenMapper {

    int deleteByPrimaryKey(Long userId);

    int insert(MallUserToken record);

    int insertSelective(MallUserToken record);

    MallUserToken selectByPrimaryKey(Long userId);

    MallUserToken selectByToken(String token);

    int updateByPrimaryKeySelective(MallUserToken record);

    int updateByPrimaryKey(MallUserToken record);
}

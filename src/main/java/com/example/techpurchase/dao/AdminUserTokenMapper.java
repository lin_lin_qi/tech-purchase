package com.example.techpurchase.dao;

import com.example.techpurchase.entity.AdminUserToken;

/**
 * @author 林佳链
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/8 23:17
 */
public interface AdminUserTokenMapper {

    int deleteByPrimaryKey(Long userId);

    int insert(AdminUserToken record);

    int insertSelective(AdminUserToken record);

    AdminUserToken selectByPrimaryKey(Long userId);

    AdminUserToken selectByToken(String token);

    int updateByPrimaryKeySelective(AdminUserToken record);

    int updateByPrimaryKey(AdminUserToken record);

}

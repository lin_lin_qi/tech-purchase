package com.example.techpurchase.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

/**
 * @author 林佳链
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/8 14:27
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                //指定文档接口路径
                .apis(RequestHandlerSelectors.basePackage("com.example.techpurchase.api"))
                .build();
    }

    //文档简介
    private ApiInfo apiInfo(){

        //作者信息
        Contact contact = new Contact(
                "林佳链、陈思杭、李乐雅、黄惠桃", "http://localhost:28019", "285039694@qq.com");

        return new ApiInfo(
                "采购系统接口文档",
                "待会描述",
                "1.0",
                "http://localhost:28019",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList()
                );
    }

}

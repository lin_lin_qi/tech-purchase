
package com.example.techpurchase.config.annotation;

import java.lang.annotation.*;


/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/21
 */



@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TokenToMallUser {

    /**
     * 当前用户在request中的名字
     *
     * @return
     */
    String value() default "user";

}

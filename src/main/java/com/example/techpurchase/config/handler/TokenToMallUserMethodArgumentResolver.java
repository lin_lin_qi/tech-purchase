package com.example.techpurchase.config.handler;

import com.example.techpurchase.config.annotation.TokenToMallUser;
import com.example.techpurchase.dao.MallUserTokenMapper;
import com.example.techpurchase.entity.MallUserToken;
import com.example.techpurchase.exception.TechPurchaseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


@Component
public class TokenToMallUserMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Autowired
    private MallUserTokenMapper mallUserTokenMapper;

    public TokenToMallUserMethodArgumentResolver() {
    }

    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(TokenToMallUser.class);
    }

    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        if (parameter.getParameterAnnotation(TokenToMallUser.class)!=null) {
            String token = webRequest.getHeader("token");
            if (null != token && !"".equals(token) && token.length() == 32) {
                MallUserToken mallUserToken = mallUserTokenMapper.selectByToken(token);
                if (mallUserToken == null) {
                    TechPurchaseException.fail("未登录！");
                } else if (mallUserToken.getExpireTime().getTime() <= System.currentTimeMillis()) {
                    TechPurchaseException.fail("无效认证！请重新登录！");
                }
                return mallUserToken;
            } else {
                TechPurchaseException.fail("未登录！");
            }
        }
        return null;
    }

}

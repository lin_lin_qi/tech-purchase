package com.example.techpurchase.config.handler;

import com.example.techpurchase.config.annotation.TokenToAdminUser;

import com.example.techpurchase.dao.AdminUserTokenMapper;
import com.example.techpurchase.entity.AdminUserToken;
import com.example.techpurchase.exception.TechPurchaseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

@Component
public class TokenToAdminUserMethodArgumentResolver implements HandlerMethodArgumentResolver {

    @Autowired
    private AdminUserTokenMapper adminUserTokenMapper;

    public TokenToAdminUserMethodArgumentResolver() {
    }

    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(TokenToAdminUser.class);
    }

    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        if (parameter.getParameterAnnotation(TokenToAdminUser.class) != null) {
            String token = webRequest.getHeader("token");
            if (null != token && !"".equals(token) && token.length() == 32) {
                AdminUserToken adminUserToken = adminUserTokenMapper.selectByToken(token);
                if (adminUserToken == null) {
                    TechPurchaseException.fail("管理员未登录");
                } else if (adminUserToken.getExpireTime().getTime() <= System.currentTimeMillis()) {
                    TechPurchaseException.fail("管理员登录过期！请重新登录！");
                }
                return adminUserToken;
            } else {
                TechPurchaseException.fail("管理员未登录");
            }
        }
        return null;
    }

}

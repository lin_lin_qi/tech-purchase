package com.example.techpurchase.api.admin.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;


/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/22
 */
@Data
public class GoodsDeleteByIdsParam {
    @ApiModelProperty("待删除商品id")
    @NotNull(message = "商品id不能为空")
    private Long[] goodIds;

    public Long[] getGoodIds() {
        return this.goodIds;
    }
}

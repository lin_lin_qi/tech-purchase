package com.example.techpurchase.api.admin;

import com.example.techpurchase.api.admin.param.AdminLoginParam;
import com.example.techpurchase.config.annotation.TokenToAdminUser;
import com.example.techpurchase.entity.AdminUser;
import com.example.techpurchase.entity.AdminUserToken;
import com.example.techpurchase.service.AdminUserService;
import com.example.techpurchase.util.Result;
import com.example.techpurchase.util.ResultGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * @author 林佳链
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/8 15:39
 */
@RestController
@Slf4j
public class AdminUserControl {

    @Resource
    private AdminUserService adminUserService;

    @ApiOperation("管理员登录")
    @PostMapping("/admin/login")
    public Result loginAdmin(@ApiParam("管理员用户名和密码")@RequestBody @Valid AdminLoginParam adminLoginParam){


       log.info("日志输出");

        String loginResult = adminUserService.login(adminLoginParam.getAdminName(), adminLoginParam.getAdminPwd());

        //登录成功
        if(!loginResult.equals("登录失败")) return ResultGenerator.genSuccessResult(loginResult);
        //登录失败
        return ResultGenerator.genFailResult(loginResult);
    }

    @ApiOperation("管理员注册")
    @PostMapping("/admin/register")
    public Result regAdmin(@ApiParam("管理员用户名和密码")@RequestBody @Valid AdminLoginParam adminRegParam){

        if(adminUserService.hasUser(adminRegParam.getAdminName())) return ResultGenerator.genFailResult("存在同名用户");

        String loginResult = adminUserService.register(new AdminUser(adminRegParam.getAdminName(), adminRegParam.getAdminPwd()));

        //注册成功
        if(!loginResult.equals("注册失败")) return ResultGenerator.genSuccessResult(loginResult);
        //注册失败
        return ResultGenerator.genFailResult(loginResult);
    }


}

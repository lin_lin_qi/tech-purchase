package com.example.techpurchase.api.admin.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/22
 */

@Data
public class GoodsDeleteByIdParam {
    @ApiModelProperty("待删除商品id")
    @NotNull(message = "商品id不能为空")
    @Min(value = 1, message = "商品id不能为空")
    private Long goodId;

    public Long getGoodId() {
        return this.goodId;
    }
}

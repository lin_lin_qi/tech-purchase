package com.example.techpurchase.api.admin;

import com.example.techpurchase.util.Result;
import com.example.techpurchase.util.ResultGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 林佳链
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/6 22:29
 */
@RestController
public class HelloControl {

    @ApiOperation("接口示例")
    @GetMapping("/hello")
    public Result hello(@ApiParam("接口参数")String hello){
        return ResultGenerator.genSuccessResult(hello);
    }

}

package com.example.techpurchase.api.admin;

import com.example.techpurchase.api.admin.param.*;
import com.example.techpurchase.config.annotation.TokenToAdminUser;
import com.example.techpurchase.entity.*;
import com.example.techpurchase.service.TechPurchaseGoodsService;
import com.example.techpurchase.util.BeanUtil;
import com.example.techpurchase.util.Result;
import com.example.techpurchase.util.ResultGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;


import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/21
 */

@Component
@RestController
@Api(tags = "管理员对商品的增删改查")
@RequestMapping(value = "/adminGoods")

public class AdminGoodsControl {

    private static final Logger logger = LoggerFactory.getLogger(AdminGoodsControl.class);
    @Resource
    private TechPurchaseGoodsService techPurchaseGoodsService;


    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiOperation(value = "管理员新增商品的接口",notes = "管理员新增商品的接口")
    public Result adminAddGoods(@RequestBody @Valid GoodsAddParam goodsAddParam, @TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        TechPurchaseGoods techPurchaseGoods = new TechPurchaseGoods();
        BeanUtil.copyProperties(goodsAddParam, techPurchaseGoods);
        Long result = techPurchaseGoodsService.adminAdd(techPurchaseGoods);
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("添加失败");
        }
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ApiOperation(value = "管理员更新商品信息的接口",notes = "管理员更新商品信息的接口")
    public Result adminUpdateGoods(@RequestBody @Valid GoodsUpdateParam goodsUpdateParam,
                                   @TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        TechPurchaseGoods techPurchaseGoods = new TechPurchaseGoods();
        BeanUtil.copyProperties(goodsUpdateParam, techPurchaseGoods);
        Long result = techPurchaseGoodsService.adminUpdate(techPurchaseGoods);
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("更新失败");
        }
    }

    @RequestMapping(value = "/deleteById",method = RequestMethod.POST)
    @ApiOperation(value = "管理员根据id删除商品的接口",notes = "管理员根据id删除商品的接口")
    public Result adminDeleteByIdGoods(@RequestBody @Valid GoodsDeleteByIdParam goodsDeleteByIdParam, @TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        Long result = techPurchaseGoodsService.adminDeleteById(goodsDeleteByIdParam.getGoodId());
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("删除失败");
        }
    }

    @RequestMapping(value = "/deleteByIds",method = RequestMethod.POST)
    @ApiOperation(value = "管理员批量删除商品的接口",notes = "管理员批量删除商品的接口")
    public Result adminDeleteByIdsGoods(@RequestBody @Valid GoodsDeleteByIdsParam goodsDeleteByIdsParam, @TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        Long result = techPurchaseGoodsService.adminDeleteByIds(goodsDeleteByIdsParam.getGoodIds());
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("删除失败");
        }
    }

    @RequestMapping(value = "/visual",method = RequestMethod.GET)
    @ApiOperation(value = "管理员可视化的接口",notes = "管理员可视化的接口")
    public Result adminVisualGoods(@TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        List<TechPurchaseVisual> result = techPurchaseGoodsService.adminVisual();
        if (result!=null) {
            return ResultGenerator.genSuccessResult(result);
        } else {
            return ResultGenerator.genFailResult("获取失败");
        }
    }

    @RequestMapping(value = "/goodsCount",method = RequestMethod.GET)
    @ApiOperation(value = "管理员获取商品数量的接口",notes = "管理员获取商品数量的接口")
    public Result adminGoodsCount(@TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        List<TechPurchaseCount> result = techPurchaseGoodsService.adminGoodsCount();
        if (result!=null) {
            return ResultGenerator.genSuccessResult(result);
        } else {
            return ResultGenerator.genFailResult("获取失败");
        }
    }

    @RequestMapping(value = "/goodsCountByQuery",method = RequestMethod.POST)
    @ApiOperation(value = "管理员获取查询后商品数量的接口",notes = "管理员获取查询后商品数量的接口")
    public Result adminGoodsCountByQuery(@RequestBody @Valid GoodsCountByQueryParam goodsCountByQueryParam, @TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        TechPurchaseQuery techPurchaseQuery = new TechPurchaseQuery();
        BeanUtil.copyProperties(goodsCountByQueryParam, techPurchaseQuery);
        List<TechPurchaseCount> result = techPurchaseGoodsService.adminGoodsCountByQuery(techPurchaseQuery);
        if (result!=null) {
            return ResultGenerator.genSuccessResult(result);
        } else {
            return ResultGenerator.genFailResult("获取失败");
        }
    }

    @RequestMapping(value = "/goodsList",method = RequestMethod.POST)
    @ApiOperation(value = "管理员分页获取商品列表的接口",notes = "管理员分页获取商品列表的接口")
    public Result adminGoodsList(@RequestBody @Valid GoodsListParam goodsListParam, @TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        TechPurchasePage techPurchasePage = new TechPurchasePage();
        BeanUtil.copyProperties(goodsListParam, techPurchasePage);
        List<TechPurchaseGoods> result = techPurchaseGoodsService.adminGoodsList(techPurchasePage);
        if (result!=null) {
            return ResultGenerator.genSuccessResult(result);
        } else {
            return ResultGenerator.genFailResult("获取失败");
        }
    }

    @RequestMapping(value = "/goodsListByQuery",method = RequestMethod.POST)
    @ApiOperation(value = "管理员经过查询分页获取商品列表的接口",notes = "管理员批量删除商品的接口")
    public Result adminGoodsListByQuery(@RequestBody @Valid GoodsListByQueryParam goodsListByQueryParam, @TokenToAdminUser AdminUserToken userToken){
        logger.info("adminUser:{}", userToken.toString());
        TechPurchasePageByQuery techPurchasePageByQuery = new TechPurchasePageByQuery();
        BeanUtil.copyProperties(goodsListByQueryParam, techPurchasePageByQuery);
        List<TechPurchaseGoods> result = techPurchaseGoodsService.adminGoodsListByQuery(techPurchasePageByQuery);
        if (result!=null) {
            return ResultGenerator.genSuccessResult(result);
        } else {
            return ResultGenerator.genFailResult("获取失败");
        }
    }


}

package com.example.techpurchase.api.mall;

import com.example.techpurchase.api.admin.param.GoodsAddParam;
import com.example.techpurchase.api.admin.param.GoodsDeleteByIdParam;
import com.example.techpurchase.api.admin.param.GoodsDeleteByIdsParam;
import com.example.techpurchase.api.admin.param.GoodsUpdateParam;
import com.example.techpurchase.config.annotation.TokenToMallUser;
import com.example.techpurchase.entity.MallUserToken;
import com.example.techpurchase.entity.TechPurchaseGoods;
import com.example.techpurchase.entity.TechPurchaseVisual;
import com.example.techpurchase.service.MallUserPurchaseGoodsService;
import com.example.techpurchase.util.BeanUtil;
import com.example.techpurchase.util.Result;
import com.example.techpurchase.util.ResultGenerator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;
import java.util.Objects;

/**
 * Description:普通用户对要采购的商品增删查改
 * Param:
 * return:
 * Author:黄惠桃
 * Date:2022/12/20
 */
@Component
@RestController
@Api(tags = "普通用户对要采购的商品的增删改查")
@RequestMapping(value = "/mallUserGoods")

public class MallUserGoodsControl {
    private static final Logger logger = LoggerFactory.getLogger(MallUserGoodsControl.class);
    @Resource
    private MallUserPurchaseGoodsService mallUserPurchaseGoodsService;


    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiOperation(value = "普通用户新增商品的接口",notes = "普通用户新增商品的接口")
    public Result userAddGoods(@RequestBody @Valid GoodsAddParam goodsAddParam, @TokenToMallUser MallUserToken userToken){
        logger.info("user:{}", userToken.toString());
        if(!Objects.equals(goodsAddParam.getPurchaseUserId(), userToken.getUserId())){
            return ResultGenerator.genFailResult("权限不足");
        }
        TechPurchaseGoods techPurchaseGoods = new TechPurchaseGoods();
        BeanUtil.copyProperties(goodsAddParam, techPurchaseGoods);
        Long result = mallUserPurchaseGoodsService.userAdd(techPurchaseGoods);
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("添加失败");
        }
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    @ApiOperation(value = "普通用户更新自己的商品信息的接口",notes = "普通用户更新自己的商品信息的接口")
    public Result userUpdateGoods(@RequestBody @Valid GoodsUpdateParam goodsUpdateParam,
                                  @TokenToMallUser MallUserToken userToken){
        logger.info("user:{}", userToken.toString());
        TechPurchaseGoods techPurchaseGoods = new TechPurchaseGoods();
        BeanUtil.copyProperties(goodsUpdateParam, techPurchaseGoods);
        Long result = mallUserPurchaseGoodsService.userUpdate(techPurchaseGoods);
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("更新失败");
        }
    }

    @RequestMapping(value = "/deleteById",method = RequestMethod.POST)
    @ApiOperation(value = "普通用户根据id删除自己增加商品的接口",notes = "普通用户根据id删除自己增加商品的接口")
    public Result userDeleteByIdGoods(@RequestBody @Valid GoodsDeleteByIdParam goodsDeleteByIdParam,
                                      @TokenToMallUser  MallUserToken userToken){
        logger.info("user:{}", userToken.getUserId());
        Long result = mallUserPurchaseGoodsService.userDeleteById(goodsDeleteByIdParam.getGoodId(),userToken.getUserId());
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("删除失败");
        }
    }

    @RequestMapping(value = "/deleteByIds",method = RequestMethod.POST)
    @ApiOperation(value = "普通用户批量删除自己的商品的接口",notes = "普通用户批量删除自己的商品的接口")
    public Result userDeleteByIdsGoods(@RequestBody @Valid GoodsDeleteByIdsParam goodsDeleteByIdsParam,
                                       @TokenToMallUser MallUserToken userToken){
        logger.info("user:{}", userToken.toString());
        Long result = mallUserPurchaseGoodsService.userDeleteByIds(goodsDeleteByIdsParam.getGoodIds(),userToken.getUserId());
        if (result!=0) {
            return ResultGenerator.genSuccessResult();
        } else {
            return ResultGenerator.genFailResult("删除失败");
        }
    }

   
    @RequestMapping(value = "/goodsCount",method = RequestMethod.GET)
    @ApiOperation(value = "普通用户获取商品数量的接口",notes = "普通用户获取商品数量的接口")
    public Result userGoodsCount(@TokenToMallUser MallUserToken userToken){
        logger.info("user:{}", userToken.toString());
        List<TechPurchaseCount> result = mallUserPurchaseGoodsService.userGoodsCount();
        if (result!=null) {
            return ResultGenerator.genSuccessResult(result);
        } else {
            return ResultGenerator.genFailResult("获取失败");
        }
    }

    @RequestMapping(value = "/goodsCountByQuery",method = RequestMethod.POST)
    @ApiOperation(value = "普通用户获取查询后商品数量的接口",notes = "普通用户获取查询后商品数量的接口")
    public Result userGoodsCountByQuery(@RequestBody @Valid GoodsCountByQueryParam goodsCountByQueryParam,
                                        @TokenToMallUser MallUserToken userToken){
        logger.info("user:{}", userToken.toString());
        TechPurchaseQuery techPurchaseQuery = new TechPurchaseQuery();
        BeanUtil.copyProperties(goodsCountByQueryParam, techPurchaseQuery);
        List<TechPurchaseCount> result = mallUserPurchaseGoodsService.userGoodsCountByQuery(techPurchaseQuery);
        if(!Objects.equals(goodsCountByQueryParam.getPurchaseUserId(), userToken.getUserId())){
            return ResultGenerator.genFailResult("权限不足");
        }
        if (result!=null) {
            return ResultGenerator.genSuccessResult(result);
        } else {
            return ResultGenerator.genFailResult("获取失败");
        }
    }

}

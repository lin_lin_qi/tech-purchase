package com.example.techpurchase.api.mall.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Description:java
 * Param:
 * return:
 * Author:黄惠桃
 * Date:2022/12/25
 */
@Data
public class GoodsListByQueryParam {
    @ApiModelProperty("前面商品数量")
    @NotNull(message = "前面商品数量不能为空")
    @Min(value = 0, message = "前面商品数量最低为0")
    private Long frontNum;


    @ApiModelProperty("一页展示商品数量")
    @NotNull(message = "一页展示商品数量不能为空")
    @Min(value = 1, message = "一页展示商品数量最低为1")
    private Long pageNum;

    @ApiModelProperty("采购状态")
    @NotEmpty(message = "采购状态不能为空")
    @Length(max = 16,message = "采购状态内容过长")
    private String purchaseState;

    @ApiModelProperty("商品名称")
    @NotEmpty(message = "商品名称不能为空")
    @Length(max = 128,message = "商品名称内容过长")
    private String goodName;

    @ApiModelProperty("采购负责人id")
    @NotNull(message = "采购负责人id不能为空")
    @Min(value = 1, message = "采购负责人id最低为1")
    private Long purchaseUserId;
}

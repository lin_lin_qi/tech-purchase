
package com.example.techpurchase.api.mall.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
public class MallUserLoginParam implements Serializable {

    @ApiModelProperty("登录名")
    @NotEmpty(message = "登录名不能为空")
    private String mallUserName;

    @ApiModelProperty("密码")
    @NotEmpty(message = "密码不能为空")
    private String mallUserPwd;
}

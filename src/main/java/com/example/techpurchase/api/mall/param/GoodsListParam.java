package com.example.techpurchase.api.mall.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/25
 */
@Data
public class GoodsListParam {
    @ApiModelProperty("前面商品数量")
    @NotNull(message = "前面商品数量不能为空")
    @Min(value = 0, message = "前面商品数量最低为0")
    private Long frontNum;


    @ApiModelProperty("一页展示商品数量")
    @NotNull(message = "一页展示商品数量不能为空")
    @Min(value = 1, message = "一页展示商品数量最低为1")
    private Long pageNum;

    @ApiModelProperty("采购负责人id")
    @NotNull(message = "采购负责人id不能为空")
    @Min(value = 1, message = "采购负责人id最低为1")
    private Long purchaseUserId;

}

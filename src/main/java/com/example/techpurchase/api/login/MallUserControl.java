package com.example.techpurchase.api.login;

import com.example.techpurchase.api.admin.AdminUserControl;
import com.example.techpurchase.api.admin.param.AdminLoginParam;
import com.example.techpurchase.config.annotation.TokenToAdminUser;
import com.example.techpurchase.entity.AdminUserToken;
import com.example.techpurchase.entity.MallUser;
import com.example.techpurchase.service.MallUserService;
import com.example.techpurchase.util.Result;
import com.example.techpurchase.util.ResultGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * @author 李乐雅
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/10 22:35
 */

@RestController
public class MallUserControl {

    @Resource
    MallUserService mallUserService;

    @ApiOperation("普通用户登录")
    @PostMapping("/login/user")
    public Result loginAdmin(@ApiParam("普通用户名和密码")@RequestBody @Valid AdminLoginParam adminLoginParam){
        String loginResult = mallUserService.login(adminLoginParam.getAdminName(), adminLoginParam.getAdminPwd());


        //登录成功
        if(!loginResult.equals("登录失败")) return ResultGenerator.genSuccessResult(loginResult);
        //登录失败
        return ResultGenerator.genFailResult(loginResult);
    }

    @ApiOperation("普通用户注册")
    @PostMapping("/login/register")
    public Result regAdmin(@ApiParam("管理员用户名和密码")@RequestBody @Valid AdminLoginParam adminRegParam){

        if(mallUserService.hasUser(adminRegParam.getAdminName())) return ResultGenerator.genFailResult("存在同名用户");

        String loginResult = mallUserService.register(new MallUser(adminRegParam.getAdminName(), adminRegParam.getAdminPwd()));

        //注册成功
        if(!loginResult.equals("注册失败")) return ResultGenerator.genSuccessResult(loginResult);
        //注册失败
        return ResultGenerator.genFailResult(loginResult);
    }

    @ApiOperation("管理员根据id查找用户名字")
    @GetMapping("/admin/findByNameMallId/{mallId}")
    public Result findByNameMallId(@TokenToAdminUser AdminUserToken userToken, @PathVariable Long mallId){

        MallUser mallUser = mallUserService.findByIdToName(mallId);

        return mallUser!=null?
                ResultGenerator.genSuccessResult(mallUser.getMallUserName()):
                ResultGenerator.genFailResult("查找失败");
    }

    @ApiOperation("管理员根据名字查找用户id")
    @GetMapping("/admin/findByNameMallId")
    public Result findByNameMallId(@TokenToAdminUser AdminUserToken userToken,String name){

        MallUser mallUser = mallUserService.findByNameToId(name);

        return mallUser!=null?
                ResultGenerator.genSuccessResult(mallUser.getMallUserId()):
                ResultGenerator.genFailResult("查找失败");
    }

}

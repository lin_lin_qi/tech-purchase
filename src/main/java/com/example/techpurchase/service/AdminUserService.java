package com.example.techpurchase.service;

import com.example.techpurchase.entity.AdminUser;
import com.example.techpurchase.entity.MallUser;

public interface AdminUserService {

    /**
     * 管理员登录
     * @param name
     * @param pwd
     * @return
     */
    String login(String name, String pwd);

    /**
     * 管理员注册
     * @param adminUser
     * @return
     */
    String register(AdminUser adminUser);

    /**
     * 查找同名管理员
     * @param name
     * @return
     */
    boolean hasUser(String name);

}

package com.example.techpurchase.service;

import com.example.techpurchase.entity.*;

import java.util.List;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/12
 */
public interface TechPurchaseGoodsService {
    /**
     * 管理员添加商品
     * @param goods
     * @return
     */
    Long adminAdd(TechPurchaseGoods goods);

    /**
     * 管理员修改商品信息
     * @param goods
     * @return
     */
    Long adminUpdate(TechPurchaseGoods goods);

    /**
     * 管理员根据id删除商品
     * @param goodId
     * @return
     */
    Long adminDeleteById(Long goodId);

    /**
     * 管理员根据id删除商品
     * @param goodIds
     * @return
     */
    Long adminDeleteByIds(Long[] goodIds);

    /**
     * 管理员查看分类和数量
     * @return
     */

    List<TechPurchaseVisual> adminVisual();

    /**
     * 全部商品的数量
     * @return
     */
    List<TechPurchaseCount> adminGoodsCount();

    /**
     * 查询到的商品数量
     * @param techPurchaseQuery
     * @return
     */
    List<TechPurchaseCount> adminGoodsCountByQuery(TechPurchaseQuery techPurchaseQuery);

    /**
     * 分页列出全部商品
     * @param techPurchasePage
     * @return
     */
    List<TechPurchaseGoods> adminGoodsList(TechPurchasePage techPurchasePage);

    /**
     * 分页查询全部商品
     * @param techPurchasePageByQuery
     * @return
     */
    List<TechPurchaseGoods> adminGoodsListByQuery(TechPurchasePageByQuery techPurchasePageByQuery);


}

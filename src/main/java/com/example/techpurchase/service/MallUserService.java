package com.example.techpurchase.service;

import com.example.techpurchase.entity.AdminUser;
import com.example.techpurchase.entity.MallUser;

/**
 * @author 林佳链
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/10 22:43
 */
public interface MallUserService {

    /**
     * 用户登录
     * @param name
     * @param pwd
     * @return
     */
    String login(String name, String pwd);

    /**
     * 用户注册
     * @param mallUser
     * @return
     */
    String register(MallUser mallUser);

    /**
     * 查找同名用户
     * @param name
     * @return
     */
    boolean hasUser(String name);

    MallUser findByNameToId(String name);


    MallUser findByIdToName(Long mallId);
}

package com.example.techpurchase.service.impl;

import com.example.techpurchase.dao.MallUserMapper;
import com.example.techpurchase.dao.MallUserTokenMapper;
import com.example.techpurchase.entity.MallUser;
import com.example.techpurchase.entity.MallUserToken;
import com.example.techpurchase.service.MallUserService;
import com.example.techpurchase.util.NumberUtil;
import com.example.techpurchase.util.SystemUtil;

import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.Date;

/**
 * @author 李乐雅
 * @version 1.0.0
 * @description TODO
 * @date 2022/12/10 22:45
 */
@Service
public class MallUserServiceImpl implements MallUserService {
    @Resource
    private MallUserMapper mallUserMapper;

    @Resource
    private MallUserTokenMapper mallUserTokenMapper;
    @Override
    public String login(String userName, String password) {
        MallUser mallUser = mallUserMapper.find(userName, password);
        if (mallUser != null) {
            //登录后即执行修改token的操作
            String token = getNewToken(System.currentTimeMillis() + "", mallUser.getMallUserId());
            MallUserToken userToken = mallUserTokenMapper.selectByPrimaryKey(mallUser.getMallUserId());
            //当前时间
            Date now = new Date();
            //过期时间
            Date expireTime = new Date(now.getTime() + 2 * 24 * 3600 * 1000);//过期时间 48 小时
            if (userToken == null) {
                userToken = new MallUserToken();
                userToken.setUserId(mallUser.getMallUserId());
                userToken.setToken(token);
                userToken.setUpdateTime(now);
                userToken.setExpireTime(expireTime);
                //新增一条token数据
                if (mallUserTokenMapper.insertSelective(userToken) > 0) {
                    //新增成功后返回
                    return token;
                }
            } else {
                userToken.setToken(token);
                userToken.setUpdateTime(now);
                userToken.setExpireTime(expireTime);
                //更新
                if (mallUserTokenMapper.updateByPrimaryKeySelective(userToken) > 0) {
                    //修改成功后返回
                    return token;
                }
            }

        }
        return "登录失败";
    }

    @Override
    public String register(MallUser mallUser) {
        mallUserMapper.insert(mallUser);
        long userId = mallUser.getMallUserId();
        if (userId>0) {
            //当前时间
            Date now = new Date();
            //过期时间
            Date expireTime = new Date(now.getTime() + 2 * 24 * 3600 * 1000);//过期时间 48 小时
            String token = getNewToken(System.currentTimeMillis() + "",userId);
            MallUserToken userToken = new MallUserToken();
            userToken.setUserId(userId);
            userToken.setToken(token);
            userToken.setUpdateTime(now);
            userToken.setExpireTime(expireTime);
            //新增一条token数据
            if (mallUserTokenMapper.insertSelective(userToken) > 0) {
                //新增成功后返回
                return token;
            }
        }
        return "注册失败";
    }

    @Override
    public boolean hasUser(String name) {
        return mallUserMapper.findByName(name) !=null;
    }

    @Override
    public MallUser findByNameToId(String name) {
        return mallUserMapper.findByName(name);
    }

    @Override
    public MallUser findByIdToName(Long mallId) {
        return mallUserMapper.selectByPrimaryKey(mallId);
    }


    /**
     * 获取token值
     *
     * @param timeStr
     * @param userId
     * @return
     */
    private String getNewToken(String timeStr, Long userId) {
        String src = timeStr + userId + NumberUtil.genRandomNum(6);
        return SystemUtil.genToken(src);
    }
}

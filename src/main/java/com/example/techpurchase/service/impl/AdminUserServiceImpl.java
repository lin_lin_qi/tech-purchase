
package com.example.techpurchase.service.impl;

import com.example.techpurchase.dao.AdminUserMapper;
import com.example.techpurchase.dao.AdminUserTokenMapper;
import com.example.techpurchase.entity.AdminUser;
import com.example.techpurchase.entity.AdminUserToken;
import com.example.techpurchase.service.AdminUserService;
import com.example.techpurchase.util.NumberUtil;
import com.example.techpurchase.util.SystemUtil;

import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.Date;

@Service
public class AdminUserServiceImpl implements AdminUserService {

    @Resource
    private AdminUserMapper adminUserMapper;

    @Resource
    private AdminUserTokenMapper adminUserTokenMapper;
    @Override
    public String login(String userName, String password) {
        AdminUser loginAdminUser = adminUserMapper.find(userName, password);
        if (loginAdminUser != null) {
            //登录后即执行修改token的操作
            String token = getNewToken(System.currentTimeMillis() + "", loginAdminUser.getAdminUserId());
            AdminUserToken userToken = adminUserTokenMapper.selectByPrimaryKey(loginAdminUser.getAdminUserId());
            //当前时间
            Date now = new Date();
            //过期时间
            Date expireTime = new Date(now.getTime() + 2 * 24 * 3600 * 1000);//过期时间 48 小时
            if (userToken == null) {
                userToken = new AdminUserToken();
                userToken.setAdminUserId(loginAdminUser.getAdminUserId());
                userToken.setToken(token);
                userToken.setUpdateTime(now);
                userToken.setExpireTime(expireTime);
                //新增一条token数据
                if (adminUserTokenMapper.insertSelective(userToken) > 0) {
                    //新增成功后返回
                    return token;
                }
            } else {
                userToken.setToken(token);
                userToken.setUpdateTime(now);
                userToken.setExpireTime(expireTime);
                //更新
                if (adminUserTokenMapper.updateByPrimaryKeySelective(userToken) > 0) {
                    //修改成功后返回
                    return token;
                }
            }

        }
        return "登录失败";
    }

    @Override
    public String register(AdminUser adminUser) {
        adminUserMapper.insert(adminUser);
        long userId = adminUser.getAdminUserId();
        if (userId>0) {
            //当前时间
            Date now = new Date();
            //过期时间
            Date expireTime = new Date(now.getTime() + 2 * 24 * 3600 * 1000);//过期时间 48 小时
            String token = getNewToken(System.currentTimeMillis() + "",userId);
            AdminUserToken userToken = new AdminUserToken();
            userToken.setAdminUserId(userId);
            userToken.setToken(token);
            userToken.setUpdateTime(now);
            userToken.setExpireTime(expireTime);
            //新增一条token数据
            if (adminUserTokenMapper.insertSelective(userToken) > 0) {
                //新增成功后返回
                return token;
            }
        }
        return "注册失败";
    }

    @Override
    public boolean hasUser(String name) {
        return adminUserMapper.findByName(name) !=null;
    }


    /**
     * 获取token值
     *
     * @param timeStr
     * @param userId
     * @return
     */
    private String getNewToken(String timeStr, Long userId) {
        String src = timeStr + userId + NumberUtil.genRandomNum(6);
        return SystemUtil.genToken(src);
    }

}

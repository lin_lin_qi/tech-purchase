package com.example.techpurchase.service.impl;

import com.example.techpurchase.dao.TechPurchaseGoodsMapper;
import com.example.techpurchase.entity.*;
import com.example.techpurchase.service.TechPurchaseGoodsService;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

/**
 * Description:java
 * Param:
 * return:
 * Author:chensihang
 * Date:2022/12/12
 */
@Service
public class TechPurchaseGoodsServiceImpl implements TechPurchaseGoodsService {
    @Resource
    private TechPurchaseGoodsMapper techPurchaseGoodsMapper;

    @Override
    public Long adminAdd(TechPurchaseGoods goods){
        return techPurchaseGoodsMapper.insert(goods);
    }

    @Override
    public Long adminUpdate(TechPurchaseGoods goods){
        return techPurchaseGoodsMapper.updateById(goods);
    }

    @Override
    public Long adminDeleteById(Long goodId){
        return techPurchaseGoodsMapper.deleteById(goodId);
    }

    @Override
    public Long adminDeleteByIds(Long[] goodIds){
        return techPurchaseGoodsMapper.deleteByIds(goodIds);
    };

    @Override
    public List<TechPurchaseVisual> adminVisual(){
        return techPurchaseGoodsMapper.visual();
    };

    @Override
    public List<TechPurchaseCount> adminGoodsCount(){return techPurchaseGoodsMapper.goodsCount();};

    @Override
    public List<TechPurchaseCount> adminGoodsCountByQuery(TechPurchaseQuery techPurchaseQuery){return techPurchaseGoodsMapper.goodsCountByQuery(techPurchaseQuery);};

    @Override
    public List<TechPurchaseGoods> adminGoodsList(TechPurchasePage techPurchasePage){return techPurchaseGoodsMapper.goodsList(techPurchasePage);};

    @Override
    public List<TechPurchaseGoods> adminGoodsListByQuery(TechPurchasePageByQuery techPurchasePageByQuery){return techPurchaseGoodsMapper.goodsListByQuery(techPurchasePageByQuery);};

}

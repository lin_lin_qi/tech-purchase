package com.example.techpurchase.service.impl;

import com.example.techpurchase.dao.AdminUserTokenMapper;
import com.example.techpurchase.dao.TechPurchaseGoodsMapper;
import com.example.techpurchase.entity.AdminUserToken;
import com.example.techpurchase.entity.TechPurchaseGoods;
import com.example.techpurchase.entity.TechPurchaseVisual;
import com.example.techpurchase.service.MallUserPurchaseGoodsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description:
 * Param:
 * return:
 * Author:黄惠桃
 * Date:2022/12/20
 */
@Service
public class MallUserPurchaseGoodsServiceImpl implements MallUserPurchaseGoodsService {
    @Resource
    private TechPurchaseGoodsMapper techPurchaseGoodsMapper;

    @Override
    public Long userAdd(TechPurchaseGoods goods) {
        return techPurchaseGoodsMapper.insert(goods);
    }

    @Override
    public Long userDeleteById(Long goodId,Long userId) {
        return techPurchaseGoodsMapper.mallDeleteById(goodId,userId);
    }

    @Override
    public Long userDeleteByIds(Long[] goodIds,Long userId) {
        return techPurchaseGoodsMapper.mallDeleteByIds(goodIds,userId);
    }

    @Override
    public List<TechPurchaseVisual> userVisual() {
        return techPurchaseGoodsMapper.visual();
    }

    @Override
    public Long userUpdate(TechPurchaseGoods goods) {
        return techPurchaseGoodsMapper.updateById(goods);
    }
    @Override
    public List<TechPurchaseGoods> userGoodsList(TechPurchasePage techPurchasePage) {
        return techPurchaseGoodsMapper.goodsList(techPurchasePage);
    }
}

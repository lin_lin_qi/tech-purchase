/*
SQLyog Ultimate v10.00 Beta1
MySQL - 8.0.28 : Database - tech_purchase
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tech_purchase` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `tech_purchase`;

/*Table structure for table `tech_purchase_admin_user` */

DROP TABLE IF EXISTS `tech_purchase_admin_user`;

CREATE TABLE `tech_purchase_admin_user` (
  `admin_user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `admin_user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '管理员名字',
  `admin_user_pwd` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '管理员密码',
  PRIMARY KEY (`admin_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;

/*Data for the table `tech_purchase_admin_user` */

insert  into `tech_purchase_admin_user`(`admin_user_id`,`admin_user_name`,`admin_user_pwd`) values (1,'20201003001','123'),(2,'林佳链','123'),(4,'林佳','123'),(5,'林','123'),(6,'林2','123'),(7,'林23','123'),(8,'林11','123');

/*Table structure for table `tech_purchase_admin_user_token` */

DROP TABLE IF EXISTS `tech_purchase_admin_user_token`;

CREATE TABLE `tech_purchase_admin_user_token` (
  `admin_user_id` bigint NOT NULL COMMENT '管理员id',
  `token` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'token值',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `expire_time` datetime DEFAULT NULL COMMENT '到期时间',
  PRIMARY KEY (`admin_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;

/*Data for the table `tech_purchase_admin_user_token` */

insert  into `tech_purchase_admin_user_token`(`admin_user_id`,`token`,`update_time`,`expire_time`) values (1,'61118d8f9034bc7793098e614e86aa52','2022-12-08 23:56:35','2022-12-10 23:56:35'),(5,'19dde1aa8d410299938bf89927c24be0','2022-12-24 21:22:07','2022-12-26 21:22:07'),(7,'30fe7fdd63e05f1f8c7eda32876eb23f','2022-12-09 00:13:18','2022-12-11 00:13:18'),(8,'b7bfdf3226e8cae98098be261ec5908a','2022-12-10 23:21:15','2022-12-12 23:21:15');

/*Table structure for table `tech_purchase_goods` */

DROP TABLE IF EXISTS `tech_purchase_goods`;

CREATE TABLE `tech_purchase_goods` (
  `good_id` bigint NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `good_category` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '商品类别',
  `good_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '商品名称',
  `purchase_user_id` bigint DEFAULT NULL COMMENT '采购负责人id',
  `purchase_num` bigint DEFAULT NULL COMMENT '采购数量',
  `purchase_state` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '采购状态0采购中 1采购完成',
  PRIMARY KEY (`good_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

/*Data for the table `tech_purchase_goods` */

insert  into `tech_purchase_goods`(`good_id`,`good_category`,`good_name`,`purchase_user_id`,`purchase_num`,`purchase_state`) values (5,'办公用品','A4纸',6,30,'1'),(6,'食品`simple_blog`','酸奶',10,10,'0'),(7,'食品','牛奶',10,10,'0'),(8,'防护用品','口罩',10,10,'0'),(9,'雨具','雨伞',3,3,'1'),(10,'装饰品','圣诞树',2,2,'0'),(12,'雨具','雨衣',5,5,'2'),(20,'电子产品','平板',6,5,'1'),(21,'食品','奥利奥',3,4,'正在采购');

/*Table structure for table `tech_purchase_mall_user` */

DROP TABLE IF EXISTS `tech_purchase_mall_user`;

CREATE TABLE `tech_purchase_mall_user` (
  `mall_user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '普通用户id',
  `mall_user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '名字',
  `mall_user_pwd` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  PRIMARY KEY (`mall_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;

/*Data for the table `tech_purchase_mall_user` */

insert  into `tech_purchase_mall_user`(`mall_user_id`,`mall_user_name`,`mall_user_pwd`) values (1,'林23','123'),(2,'林','123');

/*Table structure for table `tech_purchase_mall_user_token` */

DROP TABLE IF EXISTS `tech_purchase_mall_user_token`;

CREATE TABLE `tech_purchase_mall_user_token` (
  `mall_user_id` bigint NOT NULL COMMENT '普通用户id',
  `token` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT 'token',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `expire_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`mall_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;

/*Data for the table `tech_purchase_mall_user_token` */

insert  into `tech_purchase_mall_user_token`(`mall_user_id`,`token`,`update_time`,`expire_time`) values (1,'ccc569b9085afd8f2b4cd65d73ebcf03','2022-12-10 23:27:41','2022-12-12 23:27:41'),(2,'f278c2a6319f3a6d4cad1e8d5950cdc1','2022-12-24 20:35:56','2022-12-26 20:35:56');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
